<?php

  if(isset($_FILES['fichier']) && !empty($_FILES['fichier']['tmp_name']))
  {
  	  include_once '../modeles/dao_crud_clients.php';
      include_once '../PHPExcel/IOFactory.php';
      
      $dao_crud = new dao_crud_clients();
      $objPHPExcel=PHPExcel_IOFactory::load($_FILES['fichier']['tmp_name']);

      foreach($objPHPExcel->getWorksheetIterator() as $worksheet){

		$highestRow = $worksheet->getHighestRow();

		for($row = 2 ; $row <= $highestRow ; $row++){	

			  $dao_crud->setAccountID($worksheet->getCellByColumnandRow(0,$row)->getValue());	  
			  $dao_crud->setNom($worksheet->getCellByColumnandRow(1,$row)->getValue());
			  $dao_crud->setBranch($worksheet->getCellByColumnandRow(5,$row)->getValue());
			
             if($worksheet->getCellByColumnandRow(2,$row)->getValue() !== "NULL" && $worksheet->getCellByColumnandRow(3,$row)->getValue() !== "NULL" && $worksheet->getCellByColumnandRow(4,$row)->getValue() !== "NULL" )
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(2,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(2,$row)->getValue() == "NULL" && $worksheet->getCellByColumnandRow(3,$row)->getValue() !== "NULL" && $worksheet->getCellByColumnandRow(4,$row)->getValue() !== "NULL" )
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(3,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(2,$row)->getValue() == "NULL" && $worksheet->getCellByColumnandRow(3,$row)->getValue() == "NULL" && $worksheet->getCellByColumnandRow(4,$row)->getValue() !== "NULL" )
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(4,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(2,$row)->getValue() !== "NULL" && $worksheet->getCellByColumnandRow(3,$row)->getValue() == "NULL" &&  $worksheet->getCellByColumnandRow(4,$row)->getValue() == "NULL")
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(2,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(2,$row)->getValue() !== "NULL" && ($worksheet->getCellByColumnandRow(3,$row)->getValue() == "NULL" ||  $worksheet->getCellByColumnandRow(4,$row)->getValue()) == "NULL")
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(2,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(3,$row)->getValue() !== "NULL" && ($worksheet->getCellByColumnandRow(2,$row)->getValue() == "NULL" ||  $worksheet->getCellByColumnandRow(4,$row)->getValue()) == "NULL")
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(3,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

              if($worksheet->getCellByColumnandRow(4,$row)->getValue() !== "NULL" && ($worksheet->getCellByColumnandRow(3,$row)->getValue() == "NULL" ||  $worksheet->getCellByColumnandRow(2,$row)->getValue()) == "NULL")
              {
                  $dao_crud->setPhone($worksheet->getCellByColumnandRow(4,$row)->getValue());
                  $dao_crud->ajouter_client();
              }

	    }

	 }
  }