<?php

    include_once("../entites/clients.php");
    include_once("../modeles/connexion_to_bd.php") ;
    
    
class dao_crud_clients extends clients {
   
    
    private $req; public $resultat; private $donnees;
    private $req1; public $resultat1; private $donnees1;
    private $req4; public $resultat4; private $donnees4;
    private $req5;

    private $bd_connection1  ; 



    public function ajouter_client()
      {

       $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();

        if($this->controleChamps($this->getAccountID())){
            
         $this->req = " select * from t_client_sms where AccountID = '" .$this->getAccountID(). "'";
  
         $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die(print_r($bd_connection->getConnexion()->errorInfo()));
          
         if($this->resultat->rowCount() == 0){

              $this->req = $bd_connection->getConnexion()->prepare('INSERT INTO t_client_sms(AccountID, nom, phone, branch, status) 
                 VALUES(:ACCOUNT, :NOM, :PHONE, :BRANCH, :STATUS)') or die(print_r($this->connexion->errorInfo()));

                $this->ligne = $this->req->execute(array(
                 'ACCOUNT' => $this->getAccountID(),
                 'NOM' => $this->getNom(),
                 'PHONE' => $this->getPhone(),
                 'BRANCH' => $this->getBranch(),
                 'STATUS' => $this->getStatus() ));


               if($this->ligne == 1)
               {
                //$this->sms_welcome($this->getPhone(), $this->getBranch());
                
               }else
               {
                 echo 'echec';
               }

         }else
         {
            $this->req = $bd_connection->getConnexion()->prepare("UPDATE t_client_sms set phone = '".$this->getPhone()."' where AccountID = '".$this->getAccountID()."'");

            $ligne = $this->req->execute();

            if($ligne == 1) echo "succes";
           
         }
        
         }else
         {
           echo 'champ_error';
         }
         
        }
        
        
        
        public function liste_clients($taille)
        {
           $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();
           
           $this->req = "select * from t_client_sms where status = 1 order by nom limit ".($taille * 35).",35";
           
           $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));

           $json =  '{ "clients":
                             [';
           
                                while($this->donnees = $this->resultat->fetch())
                                {
	       
                                    $account = $this->donnees['AccountID'];
                                    $nom = $this->donnees['nom'];
                                    $phone = $this->donnees['phone'];
                                    $status = $this->donnees['status'];
                                    $id = $this->donnees['id'];


                                    $json  .=   '{"item" : {"account" : "'.$account.'" , "nom" : "'.$nom.'" , "phone" : "'.$phone.'" , "status" : "'.$status.'", "id" : "'.$id.'" , }} ,';
                                }

                                $json .= ']}';
                                echo $json;
                         
            
        }


        public function liste_clients_corbeille($taille)
        {
           $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();
           
           $this->req = "select * from t_client_sms where status = 0 order by nom limit ".($taille * 35).",35";
           
           $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));

           $json =  '{ "clients":
                             [';
           
                                while($this->donnees = $this->resultat->fetch())
                                {
         
                                    $account = $this->donnees['AccountID'];
                                    $nom = $this->donnees['nom'];
                                    $phone = $this->donnees['phone'];
                                    $status = $this->donnees['status'];
                                    $id = $this->donnees['id'];


                                    $json  .=   '{"item" : {"account" : "'.$account.'" , "nom" : "'.$nom.'" ,"phone" : "'.$phone.'" , "status" : "'.$status.'", "id" : "'.$id.'" , }} ,';
                                }

                                $json .= ']}';
                                echo $json;
              
              
            
        }
        
        
        
        public function desouscrire_client(){
            
            $bd_connection = new connexion_to_bd() ; $bd_connection->etablir_connection();
        
            $this->req = $bd_connection->getConnexion()->prepare("UPDATE t_client_sms set status = 0  where id = '".$this->getId()."'") ;
	  
            $this->ligne = $this->req->execute();

            if($this->ligne == 1) echo "bien";
            else echo "bad";
         
        }
        
        public function activer_client(){
            
            $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();
        
            $this->req = $bd_connection->getConnexion()->prepare("UPDATE t_client_sms set status = 1  where id = '".$this->getId()."'") ;
	  
            $this->ligne = $this->req->execute();
            if($this->ligne == 1) echo "bien";
            else echo "bad";
         
        }

         public function supprimer_client(){
            
            $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();
        
            $this->req = $bd_connection->getConnexion()->prepare("DELETE t_client_sms  where id = '".$this->getId()."'") ;
    
            $this->ligne = $this->req->execute();
            if($this->ligne == 1) echo "bien";
            else echo "bad";
         
        }


        public function controleChamps($account)
        {
            $bool = 1;

            if (!preg_match("#^[0-9]{16}$#", $account)) 
            {
                 $bool = 0;
            }
             

            return $bool;
        }

       


        public function compte_clients()
        {
            $bd_connection = new connexion_to_bd() ;  
            $bd_connection->etablir_connection();
            
            $this->req = " select count(AccountID) as nbre_clients from t_client_sms where status = 1 " ;
            $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));
            $this->donnees = $this->resultat->fetch();
            
            $nbre_ligne = $this->donnees['nbre_clients'] ;
            $resultat = $nbre_ligne / 35 ;
            $partie_entiere = intval($resultat) ;
             
               
              if( $resultat > $partie_entiere )
              {
                $page = $partie_entiere + 1 ;  
              }

              if( $resultat == $partie_entiere)
              {
                $page = $partie_entiere ;

              }

              echo $page;
        }


        public function compter_clients_corbeille()
        {
            $bd_connection = new connexion_to_bd() ;  
            $bd_connection->etablir_connection();
            
            $this->req = " select count(AccountID) as nbre_clients from t_client_sms where status = 0 " ;
            $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));
            $this->donnees = $this->resultat->fetch();
            
            $nbre_ligne = $this->donnees['nbre_clients'] ;
            $resultat = $nbre_ligne / 35 ;
            $partie_entiere = intval($resultat) ;
             
               
              if( $resultat > $partie_entiere )
              {
                $page = $partie_entiere + 1 ;  
              }

              if( $resultat == $partie_entiere)
              {
                $page = $partie_entiere ;

              }

              echo $page;
        }


        public function sms($date)
        {
         
              
           $this->bd_connection1 = new connexion_to_bd() ;  $this->bd_connection1->etablir_connection();

           
           $this->req = "select * from t_client_sms where status = 1";
           
           $this->resultat = $this->bd_connection1->getConnexion()->query($this->req)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
                             
                             
                             while($this->donnees = $this->resultat->fetch())
                                {
         
                                    $account = $this->donnees['AccountID'];
                                    $nom = $this->donnees['nom'];
                                    $phone = $this->donnees['phone'];
                                    $trxArray = '';
                                    $dateArray = '';
                                    $amountArray = '';
                                   
                                     
                                    $this->req1 = " select * from t_accounttrxes where AccountID = '".$account."'  AND TrxDate = '".$date."' ";

                                    $this->resultat1 = $this->bd_connection1->getConnexion()->query($this->req1)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
                                    
                                    if($this->resultat1->rowCount() > 0){

                                     
                                    $i = 0;
                                    while($this->donnees1 = $this->resultat1->fetch())
                                     { 
                                          $compteur = $this->resultat1->rowCount();
                                          $trx = $this->donnees1['TrxDescription'];
                                          $amount = $this->donnees1['Amount'];
                                          $date = $this->donnees1['TrxDate'];

                                          $trxArray .= $trx . '_';
                                          $dateArray .= $date . '_';
                                          $amountArray .= $amount . '_';

                                          $this->req2 = " select * from t_accountcustomer where AccountID = '" .$account. "'";

                                          $this->resultat2 = $this->bd_connection1->getConnexion()->query($this->req2)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));

                                          $this->donnees2 = $this->resultat2->fetch();
                                          $solde = $this->donnees2['ClearBalance'];
                                          $i++;

                                          if($i == $compteur)
                                          {
                                            $this->trx_sms($amountArray,$trxArray,$dateArray,$phone,$solde,$date);
                                            
                                          } 

                                          
                                     }   
                                   }
                                }

                               
      
        }


        public function sms_new($date)
        {
         
              
           $this->bd_connection1 = new connexion_to_bd() ;  $this->bd_connection1->etablir_connection();

           
           $this->req = "select * from t_client_sms where status = 1";
           
           $this->resultat = $this->bd_connection1->getConnexion()->query($this->req)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
                             
                             
                             while($this->donnees = $this->resultat->fetch())
                                {
         
                                    $account = $this->donnees['AccountID'];
                                    $nom = $this->donnees['nom'];
                                    $phone = $this->donnees['phone'];
                                    $trxArray = '';
                                    $dateArray = '';
                                    $amountArray = '';
                                   
                                     
                                    $this->req1 = " select * from t_accounttrxes where AccountID = '".$account."'  AND TrxDate = '".$date."' ";

                                    $this->resultat1 = $this->bd_connection1->getConnexion()->query($this->req1)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
                                    
                                    if($this->resultat1->rowCount() > 0){

                                     
                                    
                                    while($this->donnees1 = $this->resultat1->fetch())
                                     { 
                    
                                          $trx = $this->donnees1['TrxDescription'];
                                          $amount = $this->donnees1['Amount'];
                                          $date = $this->donnees1['TrxDate'];

                                        

                                          $this->req2 = " select * from t_accountcustomers where AccountID = '" .$account. "'";

                                          $this->resultat2 = $this->bd_connection1->getConnexion()->query($this->req2)  or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));

                                          $this->donnees2 = $this->resultat2->fetch();
                                          $solde = $this->donnees2['ClearBalance'];
                                       
                                            $this->trx_sms($amount,$trx,$date,$phone,$solde);
                                            
                                          

                                          
                                     }   
                                   }
                                }

                               
      
        }


        private function sms_welcome($number,$branch)
        {
           $text = '';

           if($branch == '01')
           {
                /*$text = "Dear customer, In the coming days you will be closer to your account via our SMS Banking for an exceptional price ----- Your Agency APESA -----";*/

                $text = "Dear customer,to improve the processing of your operations,you will very soon benefit a package (product) at a promotional launch rate.----Your APESA branch----";
               
           }else
           {
                 /*$text = "Cher client, Dans les prochains jours vous serez plus proche de votre compte via notre SMS Banking, pour un prix exceptionnel -----Votre Agence APESA-----";*/

                $text = "Cher client,afin d’améliorer le traitement de vos opérations,vous allez bénéficier d'un package (produit) à un tarif promotionnel.----Votre  Agence APESA----";
           }

           $data = array(
           'api_key' => '5bc84cff173f0',
           'api_secret' => '5980412cefcc721ad223c7a2e9015769dee1a4cb87a15df0b226a8fc030dc7f0',
           'to' => '+237'.$number,
           'from' => 'Apesa Fund',
           'message' => $text
           );  

          
          
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, 'https://api.camoo.cm/v1/sms.json');
           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch, CURLOPT_HEADER, 0);
           curl_setopt($ch, CURLOPT_POST, 1);
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data, '', '&'));
           curl_setopt($ch, CURLOPT_TIMEOUT, 30);
           $json = curl_exec($ch);
           $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
           $jsonArry = json_decode($json);
           $message = $jsonArry->{'_message'};

           //$count = $jsonArry->{'sms'}->{'message-count'};
           echo $message;
        }


        public function trx_sms($amountA , $trxA , $dateA , $number, $solde)
        {

           /*$explode_amount = explode("_", $amountA);
           $explode_trx = explode("_", $trxA);
           $explode_date = explode("_", $dateA);*/
           $url = "http://mmp.gtsnetwork.cloud/gts/sendsinglebulk";
           $message = '';

           if($amountA > 0) $message .= 'Credit: ';
           else $message .= 'Debit: ';
        
               $message .= ' montant:' . $amountA . '; Date: ' . $dateA . '; Solde: ' . $solde ;

           
      

          
          /*$data = array(
           'api_key' => '5bc84cff173f0',
           'api_secret' => '5980412cefcc721ad223c7a2e9015769dee1a4cb87a15df0b226a8fc030dc7f0',
           'to' => '+237'.$number,
           'from' => 'Apesa Fund',
           'message' => $message ,
           ); */

           $fields = array(  
               'phone' => '678660800',  
               'password' => 'apesa@gts',  
               'sender_id' => 'Apesa Fund',  
               'contact_list' => $number, 
               'message' => $message 
        );  

          $ch = curl_init($url); 
          curl_setopt($ch, CURLOPT_POST, 1);   
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields)); 
          curl_setopt($ch, CURLOPT_HTTPHEADER,  array('Content-Type: application/x-www-form-urlencoded')); 
          $result = curl_exec($ch);  
          $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

          if($httpCode == 200){           
            if($result == 200){
                 $jsonArry = json_decode($result);
                 echo $jsonArry;                         
            } 
          }
          /*
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, 'https://api.camoo.cm/v1/sms.json');
           curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($ch, CURLOPT_HEADER, 0);
           curl_setopt($ch, CURLOPT_POST, 1);
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data, '', '&'));
           curl_setopt($ch, CURLOPT_TIMEOUT, 30);
           $json = curl_exec($ch);
           $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
           $jsonArry = json_decode($json);


             Sauvegarde du rapport de 
                                la transaction en local
           

          /* $retour = $jsonArry->{'_message'};
           $count = $jsonArry->{'sms'}->{'message-count'};
           $price = $jsonArry->{'sms'}->{'messages'}{'0'}->{'message-price'};
           $solde = $jsonArry->{'sms'}->{'messages'}{'0'}->{'remaining-balance'};


           $this->req4 = " select * from t_client_sms where phone = '" .$number."' ";
           $this->resultat4 = $this->bd_connection1->getConnexion()->query($this->req4) or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
           $this->donnees4 = $this->resultat4->fetch();
           $nom = $this->donnees4['nom']; 


           $this->req5 = $this->bd_connection1->getConnexion()->prepare('INSERT INTO t_etat_sms(nom, phone, message_count, message_price, solde, etat_date, etat) 
                 VALUES(:NOM, :PHONE, :COUNT, :PRICE, :SOLDE, :LADATE, :ETAT)') or die(print_r($this->connexion->errorInfo()));


                $this->ligne = $this->req5->execute(array(
                 'NOM' =>  $nom,
                 'PHONE' => $number,
                 'COUNT' => $count,
                 'PRICE' => $price,
                 'SOLDE' => $solde,
                 'LADATE' => $date,
                 'ETAT' =>  $retour));*/

        }


        public function rapport($date)
        {

          $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();

          $this->req = " select * from t_etat_sms where etat_date = '".$date."' ";

            $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));
                                    
              $json =  '{ "rapport":
                             [';   

              while($this->donnees = $this->resultat->fetch())
                   { 
                      $count = $this->donnees['message_count'];
                      $nom = $this->donnees['nom'];
                      $phone = $this->donnees['phone'];
                      $price = $this->donnees['message_price'];
                      $solde = $this->donnees['solde'];
                      $ladate = $this->donnees['etat_date'];
                      $etat = $this->donnees['etat'];

                     $json  .=   '{"item" : {"count" : "'.$count.'" , "nom" : "'.$nom.'" ,"phone" : "'.$phone.'" , "price" : "'.$price.'", "solde" : "'.$solde.'" , "ladate" : "'.$date.'", "etat" : "'.$etat.'" , }} ,';

                   }

              $json .= ']}';
              echo $json;


        }

        public function rechercher_client($nameOrphone)
        {
          $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();

           $this->req = "select * from t_client_sms where (nom LIKE '".$nameOrphone."%' OR phone = '".$nameOrphone."') AND status = 1 ";
            $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));

            $json =  '{ "clients":
                             [';
            while($this->donnees = $this->resultat->fetch())
                   { 
                      $account = $this->donnees['AccountID'];
                      $nom = $this->donnees['nom'];
                      $phone = $this->donnees['phone'];
                      $branch = $this->donnees['branch'];
                      $id = $this->donnees['id'];

                     $json  .=   '{"item" : {"account" : "'.$account.'" , "nom" : "'.$nom.'" ,"phone" : "'.$phone.'" , "branch" : "'.$branch.'","id" : "'.$id.'" ,}} ,';
                   }

              $json .= ']}';
              echo $json;

        }

        public function sms_masse($message,$lang)
        {
            $bd_connection = new connexion_to_bd() ;  $bd_connection->etablir_connection();

            if($lang == "01")
            {
              $this->req = "select * from t_client_sms where branch = 01 AND status = 1 ";
              $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));
            }
            else
            {
               $this->req = "select * from t_client_sms where branch != 01 AND branch != 00 AND status = 1 ";
              $this->resultat = $bd_connection->getConnexion()->query($this->req)  or die( print_r($bd_connection->getConnexion()->errorInfo()));
            }

            while($this->donnees = $this->resultat->fetch())
            {

              $data = array(
              'api_key' => '5bc84cff173f0',
              'api_secret' => '5980412cefcc721ad223c7a2e9015769dee1a4cb87a15df0b226a8fc030dc7f0',
              'to' => '+237'.$this->donnees['phone'],
              'from' => 'Apesa Fund',
              'message' => $message ,
              );  

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.camoo.cm/v1/sms.json');
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data, '', '&'));
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $json = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $jsonArry = json_decode($json);

                /*  Sauvegarde du rapport de 
                                la transaction en local
           */

           $retour = $jsonArry->{'_message'};
           /*$count = $jsonArry->{'sms'}->{'message-count'};
           $price = $jsonArry->{'sms'}->{'messages'}{'0'}->{'message-price'};
           $solde = $jsonArry->{'sms'}->{'messages'}{'0'}->{'remaining-balance'};


           $this->req4 = " select * from t_client_sms where phone = '" .$$this->donnees['phone']."' ";
           $this->resultat4 = $this->bd_connection1->getConnexion()->query($this->req4) or die( print_r($this->bd_connection1->getConnexion()->errorInfo()));
           $this->donnees4 = $this->resultat4->fetch();
           $nom = $this->donnees4['nom'];


           $this->req5 = $bd_connection->getConnexion()->prepare('INSERT INTO t_etat_sms(AccountID, nom, phone, message_count, message_price, solde, etat_date, etat) 
                 VALUES(:ACCOUNT, :NOM, :PHONE, :COUNT, :PRICE, :SOLDE, :LADATE, :ETAT)') or die(print_r($this->connexion->errorInfo()));


                $this->ligne = $this->req5->execute(array(
                 'ACCOUNT' =>  $this->donnees['AccountID'],
                 'NOM' =>  $this->donnees['nom'],
                 'PHONE' => $this->donnees['phone'],
                 'COUNT' => $count,
                 'PRICE' => $price,
                 'SOLDE' => $solde,
                 'LADATE' => date('Y-m-d'),
                 'ETAT' =>  $retour));*/

            echo $retour;

            }

            
        }


}

?>
