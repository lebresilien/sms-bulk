
$(function(){
    $("#datePicker").datepicker({ dateFormat: "yy-mm-dd" }).val();
    $('[name="datePicker"]').datepicker({ dateFormat: "yy-mm-dd" }).val();

    $( "#datePicker" ).blur(function(){

    	var date = $(this).val();
    	if(date != ' ') $('[name="envoyer_sms"]').removeAttr('disabled');

    	
    })

    $('[name="envoyer_sms"]').click(function(){

    	var date = $('#datePicker').val();
    	sms(date);
        
    })

    $('[name="rechercher_rapport"]').click(function(){

        var date = $('[name="datePicker"]').val();
        rapport(date);
        
    })

    $('[name="found_element"]').click(function(){

        var nameOrphone = $('[name="nameOrphone"]').val();
        if(nameOrphone.length > 3 )
        {
            rechercher_client(nameOrphone)
        }
        
    })

    /* gestion des sms en masse  */

    $('[name="sms_masse"]').click(function(){
       
       let message = $('#message').val();
       let lang = $('[name="lang"]:checked').val();

       if(message.length > 20)
       {
        $('<div id="dialogue" title="Confirmation">Voulez-vous vraiment effectuer cette operation ?</div>')
            .dialog({

                 modal:true,
                 buttons:{
              
                    "Oui":function(){
                     sms_masse(message,lang);
                     $(this).dialog("close");
                     },
                     "Non":function(){
                      $(this).dialog("close"); 
                      }
                  }
            });
       }
    })

});


function sms_masse(message,lang)
{
    var xhr = getXMLHttp() ;
    var url = "../controleurs/sms_masse.php";

    xhr.onreadystatechange = function()
        {
            $('[name="sms_masse"]').text('Patienter...')         
            if(xhr.readyState == 4 )
             {

                if(xhr.status == 200)
                  {
                    $('[name="sms_masse"]').text('Envoyer')
                    var info = xhr.responseText;alert(info)
                    if(info.indexOf('succes') != -1 )
                    {
                         setTimeout(function() {
                            toastr.success('Les sms ont été envoyés avec Succes', 'Success Alert', {timeOut: 5000});
                          }, 500);
                        
                    }else
                    {
                       setTimeout(function() {
                            toastr.error('Une erreur est survenue pendant le traitement!!!', 'Erreur Alert', {timeOut: 5000});
                          }, 500);
                    }                                
                  }
             }     
        }

        xhr.open("POST", url , false);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send("action=1&message="+message+"&lang="+lang);
}


function sms(date)
    {
    	 var xhr = getXMLHttp() ;
         var url = "../controleurs/sms.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     
                                     
                                    if(info.indexOf('200') != -1 )
                                    {
                                         setTimeout(function() {
                                            toastr.success('Les sms ont été envoyés avec Succes', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                        
                                    }else
                                    {
                                       setTimeout(function() {
                                            toastr.error('Une erreur est survenue pendant le traitement!!!', 'Erreur Alert', {timeOut: 5000});
                                          }, 500);
                                    }
                                     
                                 }
                             }     
                     }

                     xhr.open("POST", url , false);
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&date="+date);
    }


    function rapport(date)
    {
        
         var xhr = getXMLHttp() ;
         var url = "../controleurs/rapport.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     var json = eval('('+info+')');
                                     placer_rapport(json);
                                    
                                 }
                             }     
                     }

                     xhr.open("POST", url , false);
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&date="+date);
    }


function placer_rapport(json)
  {
    $('[name="tab_rapport"]').empty();$('#btn_excel').empty();
    
    $('<thead class="thead-dark">\n\
           <tr>\n\
                <td>Nom</td>\n\
                <td>Phone</td>\n\
                <td>Price</td>\n\
                <td>Count</td>\n\
                <td>Solde</td>\n\
                <td>Etat</td>\n\
                <td>Date</td>\n\
           </tr>\n\
        </thead>').appendTo('[name="tab_rapport"]');

    $('<tbody name="body_rapport"></tbody>').appendTo('[name="tab_rapport"]');

    var solde;
    var count;
    var nom;
    var phone;
    var etat;
    var ladate;
    var price;

    for(var i = 0 ; i < json.rapport.length ; i++)
    {
        solde = json.rapport[i].item.solde;
        count = json.rapport[i].item.count;
        nom = json.rapport[i].item.nom;
        phone = json.rapport[i].item.phone;
        etat = json.rapport[i].item.etat;
        price = json.rapport[i].item.price;
        ladate = json.rapport[i].item.ladate;

        $('<tr name="'+ladate+'" class="tr_etat">\n\
            <td>'+nom+'</td>\n\
            <td>'+phone+'</td>\n\
            <td>'+price+'</td>\n\
            <td>'+count+'</td>\n\
            <td>'+solde+'</td>\n\
            <td>'+etat+'</td>\n\
            <td>'+ladate+'</td>\n\
        </tr>').appendTo('[name="body_rapport"]');

    }

    $('<i class="fas fa-file-excel" style="font-size:55px;cursor:pointer;color:blue" title="cliquer pour générer l\'etat" name="etat_excel"></i>\n\
    ').appendTo('#btn_excel');

    var date = $('.tr_etat').attr('name');

    $('<form method="POST" action="../controleurs/etatExcel.php" style="display:none;"><input type="text" name="date" value="'+date+'"><input type="text" name="action" value="1"><input type="submit" name="excel_form"></form>').appendTo('#btn_excel');


    $('[name="etat_excel"]').click(function(){

      $('[name="excel_form"]').trigger('click');
    })
  }


  function etatExcel(date)
  {

         var xhr = getXMLHttp() ;
         var url = "../controleurs/etatExcel.php?action=1&ladate="+date;
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     
                                 }
                             }     
                     }

                     xhr.open("GET", url , false);
                     xhr.send(null);

  }

  function rechercher_client(content)
  {
         var xhr = getXMLHttp() ;
         var url = "../controleurs/rechercher_client.php"; 
         xhr.onreadystatechange = function()
                     {
                        $('[name="found_element"]').html('Veuillez Patienter ...')
                         if(xhr.readyState == 4 )
                             {
                              if(xhr.status == 200)
                                 {
                                     $('[name="found_element"]').html('Rechercher')
                                     var info = xhr.responseText;
                                     var json = eval('('+info+')');
                                     placer_results(json);
                                    
                                 }
                             }     
                     }

                     xhr.open("POST", url , true);
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&nameOrphone="+content);
  }

  function placer_results(json)
  {
    $('[name="placer_recherche"]').empty();

    $('<thead class="thead-dark">\n\
           <tr>\n\
                <td>AccountID</td>\n\
                <td>Nom</td>\n\
                <td>Phone</td>\n\
                <td>Branch</td>\n\
                <td>Desouscrire</td>\n\
           </tr>\n\
    </thead>').appendTo('[name="placer_recherche"]');

     $('<tbody name="body_recherche"></tbody>').appendTo('[name="placer_recherche"]');
    
    var account;
    var branch;
    var nom;
    var phone;
    var id;

    for(var i = 0 ; i < json.clients.length ; i++)
    {
        account = json.clients[i].item.account;
        nom = json.clients[i].item.nom;
        phone = json.clients[i].item.phone;
        branch = json.clients[i].item.branch;
        id = json.clients[i].item.id;

        $('<tr  class="tr_etat">\n\
            <td>'+account+'</td>\n\
            <td>'+nom+'</td>\n\
            <td>'+phone+'</td>\n\
            <td>'+branch+'</td>\n\
            <td name='+id+' etat="desous'+id+'"><center><i class="fas fa-trash" data-toggle="modal" data-target="#exampleModal" style="color:red;font-size:35px;cursor:pointer;"></i></center></td>\n\
        </tr>').appendTo('[name="body_recherche"]');

    }

    $('i').each(function(){

        $(this).click(function(){

            let id = $(this).parent().parent().attr('name');
            $('<div id="dialogue" title="Confirmation">Voulez-vous vraiment effectuer cette operation ?</div>')
            .dialog({

                 modal:true,
                 buttons:{
              
                    "Oui":function(){
                     desouscrire_client(id);
                     $(this).dialog("close");
                     },
                     "Non":function(){
                      $(this).dialog("close"); 
                      }
                  }
            });
        })
    })

  }

  function desouscrire_client(id)
  {
    var xhr = getXMLHttp() ;
    var url = "../controleurs/desouscrire_client.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     
                                     if(info == "bien"){
                                        
                                        //Retirer le td supprimer du tableau
                                        $('[etat="desous'+id+'"]').parent().remove();

                                        setTimeout(function() {
                                            toastr.success('Operation Effectuee avec Succes!!', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "bad"){
                                        
                                        setTimeout(function() {
                                            toastr.error('Une erreur innatendue est survenue!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                     }
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&id="+id);
}



  

