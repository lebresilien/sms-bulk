

$(function(){

     	$( "#onglets" ).tabs({
        active :0,                 
        event: "click",      
        effect: "explode",    
        duration: 1000,
        hide:'fade', //,{ effect: "explode", duration: 1000 }
        show: { effect: "blind", duration: 800 }, //'fade'

    });

    $('[name="importer"]').click(function(){
        
          $('[name="fichier"]').trigger('click');
          $('[name="fichier"]').change(function(){

              var allowedTypes = ['xls', 'csv', 'xlsx'];
              var fichier = $('input[type="file"]').get(0).files[0];
              var imgType = fichier.name.split('.');

              imgType = imgType[imgType.length - 1].toLowerCase();

              if(allowedTypes.indexOf(imgType) != -1) {
                  
                   if(fichier.size <= 1000000 ) 
                   {
                      //$('[name="excel"]').trigger('submit');
                      var oData = new FormData(document.forms.namedItem("excel"));
                      $.ajax({
                                url: "../controleurs/new_save_clients_excel.php",
                                type: "POST",
                                data: oData,
                                processData: false,  
                                contentType: false,   

                                success:function(data){
                                        alert(data);
                                    if(data.indexOf('champ_error') != -1 )
                                    {
                                    	setTimeout(function() {
                                            toastr.error('Veuillez Remplir Correctement les Champs!!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                    }

                                     if(data.indexOf('echec') != -1 )
                                    {
                                      setTimeout(function() {
                                            toastr.error('Une erreur innattendue est survenue!!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                    }
                                    
                                    if(data.indexOf('succes') != -1 )
                                    {
                                    	setTimeout(function() {
                                            toastr.success('Importation Terminée avec Succes', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                    }
                                }               
                                       
                       });

                   }else
                   {
                      setTimeout(function() {
                        toastr.error('Votre fichier est tres volumineux!!!!!!' , 'Error Alert', {timeOut: 5000});
                       }, 500);
                   }
              }
              else
              {
              	    setTimeout(function() {
                        toastr.error('Extension Non Valide!!!!!!' , 'Error Alert', {timeOut: 5000});
                    }, 500);
              }
              
          })
    });


     $('[name="Enregistrer"]').click(function(){
        
         var account = $('#account').val();
         var nom = $('#nom').val();
         var phone = $('#phone').val();
         var branch = $('#branch').val();

         if(verificationChamps(nom , account , phone))
         {
            SaveClients(account , nom , phone , branch);
         }else
         {
            setTimeout(function() {
                  toastr.error('Veuillez Remplir Correctement les champs!!!', 'Error Alert', {timeOut: 5000});
            }, 500);
         }

        //SaveClients(account , nom , phone , branch);
    });

  /* Mettre le premiere des champs nom et prenom
                      en majuscule quand ils perdent le focus */

     $('#nom').blur(function(){

            var nom = $(this).val();
            var maj = letterMajuscule(nom);
            $(this).val(maj);  
    });

    /* N'activer le boutton de soumission seulement 
                      si tous les champs sont bien remplis */

     /*$("input").keypress(function(){

         var account = $('#account').val();
         var nom = $('#nom').val();
         var phone = $('#phone').val();

     	 verificationChamps(nom , account , phone);
     });*/


      /* Apres le click sur l'element aller chercher la 
                      liste des clients ayant souscrits */

      $('[name="liste"]').click(function(){
          
          listeClients();
          $('[name="pagination"]').empty();
          compte_nbre_client();
      });


       /* Apres le click sur l'element aller chercher la 
                      liste des clients en corbeille */


      $('[name="corbeille"]').click(function(){
          
          
          listeClients_corbeille();
          $('[name="pagination_corbeille"]').children().empty();
          compte_nbre_client_corbeille();
      });

})


function SaveClients(account , nom , phone , branch)
{

	     var xhr = getXMLHttp() ;
         var url = "../controleurs/save_clients.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                      
                                    if(info.indexOf('Unkown') != -1 )
                                    {
                                      setTimeout(function() {
                                            toastr.error('Une Erreur innatendue est survenue.Verifiez vos parametres sms!!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                    }

                                    if(info == "succes"){

                                         setTimeout(function() {
                                            toastr.success('Client ajoute avec succes.Un sms lui sera envoyé', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }
                                    
                                     if(info.message == "succes"){

                                         setTimeout(function() {
                                            toastr.success('Client ajoute avec succes.Un sms lui sera envoyé', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "existe"){

                                         setTimeout(function() {
                                            toastr.error('Ce client existe deja', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "echec"){

                                         setTimeout(function() {
                                            toastr.error('Une erreur innatendue est survenue!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "champ_error"){

                                         setTimeout(function() {
                                            toastr.error('Veuillez Remplir Correctement les Champs!!!', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }
                                     
                                    
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("account="+account+"&nom="+nom+"&phone="+phone+"&branch="+branch);
                    
                     
}

function letterMajuscule(letter)
{
    var mots=letter.substring(1,letter.length)
    var premier=letter.substring(0,1);
    var premiere=premier.toUpperCase();
   
       return premiere+mots;
}


function verificationChamps(nom , account , phone)
{
	var regexPhone = /^6[5-9][0-9]{7}$/;
	var regexNom = /^[a-zA-Z ]{3,}$/;
	var regexAccount = /^[0-9]{16}$/;
  var bool = false;

	if(regexNom.test(nom) && regexAccount.test(account) && regexPhone.test(phone)) { 
		   bool = true;
	}

   return bool;
}

/*function gestionFichier(fichier)
{
	 var allowedTypes = ['png', 'jpg', 'jpeg', 'gif'];
	

     var imgType = fichier.name.split('.');
     imgType = imgType[imgType.length - 1].toLowerCase();

     if (allowedTypes.indexOf(imgType) != -1) {

            

        }

}*/

function listeClients()
{
	 var xhr = getXMLHttp() ;
     var url = "../controleurs/liste_clients.php?action=1&taille=0";

       xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;alert(info);
                                     var json = eval('('+info+')');
                                     placer_client(json);
                                    
                                 }
                             }     
                     }

       xhr.open("GET", url , false);
       xhr.send(null);
}

function listeClients_corbeille()
{
	 var xhr = getXMLHttp() ;
     var url = "../controleurs/liste_clients_corbeille.php?action=1&taille=0";

       xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     var json = eval('('+info+')');
                                     placer_client_corbeille(json);
                                    
                                 }
                             }     
                     }

       xhr.open("GET", url , true);
       xhr.send(null);
}

function listeClients_b(taille)
{
	 var xhr = getXMLHttp() ;
     var url = "../controleurs/liste_clients.php?action=1&taille="+taille;

       xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     var json = eval('('+info+')');
                                     placer_client(json);
                                    
                                 }
                             }     
                     }

       xhr.open("GET", url , false);
       xhr.send(null);
}

function listeClients_c(taille)
{
	 var xhr = getXMLHttp() ;
     var url = "../controleurs/liste_clients_corbeille.php?action=1&taille="+taille;

       xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     var json = eval('('+info+')');
                                     placer_client_corbeille(json);
                                    
                                 }
                             }     
                     }

       xhr.open("GET", url , false);
       xhr.send(null);
}


function placer_client(json)
{
	$('[name="container_tab"]').children().empty();

	var id;
	var account;
	var nom;
	var phone;
	var status;

	for(var i = 0 ; i < json.clients.length ; i++)
	{
		id = json.clients[i].item.id;
		account = json.clients[i].item.account;
		nom = json.clients[i].item.nom;
		phone = json.clients[i].item.phone;
		status = json.clients[i].item.status;

		$('<tr>\n\
			<td>'+account+'</td>\n\
			<td>'+nom+'</td>\n\
			<td>'+phone+'</td>\n\
			<td name='+id+' etat="desous'+id+'"><center><i class="fas fa-trash" data-toggle="modal" data-target="#exampleModal" style="color:red;font-size:35px;cursor:pointer;"></i></center></td>\n\
		</tr>').appendTo('[name="container_tab"]');
	}


	$('i').each(function(){

		$(this).click(function(){

			let id = $(this).parent().parent().attr('name');
			$('<div id="dialogue" title="Confirmation">Voulez-vous vraiment effectuer cette operation ?</div>')
			.dialog({

                 modal:true,
                 buttons:{
              
                    "Oui":function(){
                     desouscrire_client(id);
                     $(this).dialog("close");
                     },
                     "Non":function(){
                      $(this).dialog("close"); 
                      }
                  }
			});
		})
	})
}

function placer_client_corbeille(json)
{
	$('[name="container_tab_corbeille"]').children().empty();

	var id;
	var account;
	var nom;
	var phone;
	var status;

	for(var i = 0 ; i < json.clients.length ; i++)
	{
		id = json.clients[i].item.id;
		account = json.clients[i].item.account;
		nom = json.clients[i].item.nom;
		phone = json.clients[i].item.phone;
		status = json.clients[i].item.status;

		$('<tr>\n\
			<td>'+account+'</td>\n\
			<td>'+nom+'</td>\n\
			<td>'+phone+'</td>\n\
			<td name='+id+' etat="activer'+id+'"><center><i class="fal fa-lock-open" data-toggle="modal" name="activer" data-target="#exampleModal" style="font-size:35px;cursor:pointer;"></i></center></td>\n\
			<td name='+id+' etat="desous'+id+'"><center><i class="fas fa-trash" data-toggle="modal" name="delete" data-target="#exampleModal" style="color:red;font-size:35px;cursor:pointer;"></i></center></td>\n\
		</tr>').appendTo('[name="container_tab_corbeille"]');
	}


	$('[name="delete" ]').each(function(){

		$(this).click(function(){

			let id = $(this).parent().parent().attr('name');
			$('<div id="dialogue" title="Confirmation">Voulez-vous vraiment supprimer ce client?</div>')
			.dialog({

                 modal:true,
                 buttons:{
              
                    "Oui":function(){
                     //supprimer_client(id);
                     $(this).dialog("close");
                     },
                     "Non":function(){
                      $(this).dialog("close"); 
                      }
                  }
			});
		})
	})


	$('[name="activer" ]').each(function(){

		$(this).click(function(){

			let id = $(this).parent().parent().attr('name');
			$('<div id="dialogue2" title="Confirmation">Voulez-vous vraiment Effectuer cette operation?</div>')
			.dialog({

                 modal:true,
                 buttons:{
              
                    "Oui":function(){
                     activer_client(id);
                     
                     $(this).dialog("close");
                     },
                     "Non":function(){
                      $(this).dialog("close"); 
                      }
                  }
			});
		})
	})
}

function compte_nbre_client()
{
	     var xhr = getXMLHttp() ;
         var url = "../controleurs/compter_nbre_clients.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     paginate(info);
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1");
}


function paginate(info)
{
	var infos = parseInt(info);

	for(var i = 0 ; i < infos ; i++)
	{
		let j = i + 1;
		$('<li class="page-item" name="'+i+'"><a class="page-link" href="#">'+j+'</a></li>').appendTo('[name="pagination"]');
	}

	$('li:first-child').addClass('active');

    $(".page-item").each(function(){

    	$(this).click(function(){
            
        $('li').removeClass('active');
    		var page =  $(this).attr('name');
    		$(this).addClass('active');
            listeClients_b(page);
    	})
    })
}

function paginate_corbeille(info)
{
	var infos = parseInt(info);

	for(var i = 0 ; i < infos ; i++)
	{
		let j = i + 1;
		$('<li class="page-item" name="'+i+'" etat="corbeille" ><a class="page-link" href="#">'+j+'</a></li>').appendTo('[name="pagination_corbeille"]');
	}

	$('[etat="corbeille"]:first-child').addClass('active');

    $('[etat="corbeille" ]').each(function(){

    	$(this).click(function(){
            
            $('[etat="corbeille"]').removeClass('active');
    		var page =  $(this).attr('name');
    		$(this).addClass('active');
            listeClients_c(page);
    	})
    })
}


function desouscrire_client(id)
{
	var xhr = getXMLHttp() ;
    var url = "../controleurs/desouscrire_client.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     
                                     if(info == "bien"){
                                        
                                        //Retirer le td supprimer du tableau
                                        $('[etat="desous'+id+'"]').parent().remove();

                                        setTimeout(function() {
                                            toastr.success('Operation Effectuee avec Succes!!', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "bad"){
                                     	
                                     	setTimeout(function() {
                                            toastr.error('Une erreur innatendue est survenue!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                     }
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&id="+id);
}

function compte_nbre_client_corbeille()
{
	     var xhr = getXMLHttp() ;
         var url = "../controleurs/compter_nbre_clients_corbeille.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     paginate_corbeille(info);
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1");
}

  /* Fonction qui reactiver 
                      un Client et sort de la corbeille*/


function activer_client(id)
{
	var xhr = getXMLHttp() ;
    var url = "../controleurs/activer_clients.php";
           
         xhr.onreadystatechange = function()
                     {
                         
                         if(xhr.readyState == 4 )
                             {

                              if(xhr.status == 200)
                                 {
                                     var info = xhr.responseText;
                                     
                                     if(info == "bien"){
                                        
                                        //Retirer le td supprimer du tableau
                                        $('[etat="activer'+id+'"]').parent().remove();

                                        setTimeout(function() {
                                            toastr.success('Operation Effectuee avec Succes!!', 'Success Alert', {timeOut: 5000});
                                          }, 500);
                                     }

                                     if(info == "bad"){
                                     	
                                     	setTimeout(function() {
                                            toastr.error('Une erreur innatendue est survenue!!', 'Error Alert', {timeOut: 5000});
                                          }, 500);
                                     }
                                 }
                             }     
                     }

                     xhr.open("POST", url , false)
                     xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                     xhr.send("action=1&id="+id);
}


